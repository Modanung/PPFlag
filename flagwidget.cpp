#include "flagwidget.h"

#include <QTimer>
#include <QKeyEvent>
#include <QMouseEvent>
#include <QPainter>
#include <QSvgRenderer>
#include <QStyleOptionGraphicsItem>
#include <cmath>
#include <algorithm>

#include <QDebug>

FlagWidget::FlagWidget(QWidget* parent): QWidget(parent),
    rendsvg_{ QString(":/P.svg") },
    background_{ 512, 256, QImage::Format_RGB666 },
    picker_{ 384, 384, QImage::Format_RGB666 },
    help_{},
    logoSize_{ 256 },
    logoX_{ 512 },
    logoY_{ 384 },
    epiX_{ 0 },
    epiY_{ 0 },
    circleRadius_{ 128.0 },
    ticks_{ 0 },
    displayHelp_{ false },

//    flag_color{ 90, 170, 0 }, //GREEN
    flagColor_{ 80, 20, 160 }, //PURPLE

    bright_{ 0 },
    satura_{ 255 },

    dist_{ 0 },
    wave_{ 0 },
    dith_{ 0 },

    fullscreen_{ true },
    paused_{ false },

    up_{ false },
    down_{ false },
    left_{ false },
    right_{ false },

    moving_{ false },
    scaling_{ false },
    picking_{ false },
    prevPos_{}
{
    if (fullscreen_)
        setWindowState(Qt::WindowFullScreen);

    //Construct help text
    help_.setDefaultFont({ "Lucia Grande", 12 });
    help_.setDefaultStyleSheet({ "p { color: black; }" });
    help_.setHtml("<p>F1: Display this help<br>"
                 "Arrow keys: Position logo<br>"
                 "Scroll wheel: Change logo size<br>"
                 "C: Toggle color picker<br>"
                 "B: Big logo<br>"
                 "N: Normal size logo<br>"
                 "M: Mini logo<br>"
                 "F: Toggle fullscreen<br>"
                 "P: Pause wave<br>"
                 "Q: Quit<br></p>");

    //Construct color picker image
    for (int i{ 0 }; i < picker_.width();  ++i)
    for (int j{ 0 }; j < picker_.height(); ++j)
    {
        QColor color{};
        QLineF line{ QPoint{ picker_.width() / 2, picker_.height() / 2}, QPoint{i, j} };
        int dist{ static_cast<int>(floor((line.length() - 128) * 8)) };

        if (dist <= 0)
        {
            satura_ = 255;
        }
        else if (dist > 0 && dist <= 255)
        {
            bright_ = dist;
            satura_ = 255;
        }
        else if (dist > 255 && dist <= 511)
        {
            bright_ = 255;
            satura_ = 255 - (dist - 256);
        }
        else
        {
            bright_ = 255;
            satura_ =   0;
        }

        color.setHsv(line.angle(), satura_, bright_);
        picker_.setPixel(i, j, color.rgb());
    }

    background_.fill(flagColor_);
    logoX_ = width()  / 2;
    logoY_ = height() / 2;

    //Start time
    QTimer* const timer{ new QTimer{ this } };
    connect(timer, SIGNAL(timeout()), this, SLOT(onTimer()));
    timer->setInterval(34);
    timer->start();
}

void FlagWidget::onTimer()
{
    //Wave flag
    epiX_ = background_.width()  * (logoX_ / width());
    epiY_ = background_.height() * (logoY_ / height());

    if (!paused_ && !scaling_ && !moving_)
    {
        for (int i{ 0 }; i < background_.width(); ++i)
        for (int j{ 0 }; j < background_.height(); ++j) {

            QLineF line{ QPoint{static_cast<int>(epiX_),
                                static_cast<int>(epiY_)},
                         QPoint{static_cast<int>(i),
                                static_cast<int>(j)} };

            dist_ = .08 * ( -line.length() + ticks_);
            wave_ = 14.0 * sine(dist_);//sine[dist % 512];
            dith_ = rand() % 3;

            const char red_  { 14 + dith_ + wave_ + (flagColor_.red()   * 0.88) };
            const char green_{ 14 + dith_ + wave_ + (flagColor_.green() * 0.88) };
            const char blue  { 14 + dith_ + wave_ + (flagColor_.blue()  * 0.88) };

            QRgb pixel{ qRgb(red_, green_, blue) };
            background_.setPixel(i, j, pixel);
        }
    }

    ++ticks_;

    //Shrink logo as needed
    if (logoSize_ > width()  - 64) logoSize_ = width() - 64;
    if (logoSize_ > height() - 64) logoSize_ = height() - 64;

    //Pulse circle
    circleRadius_ = (logoSize_ / 2)- logoSize_ / 32 + (logoSize_ / 64 * sin(0.025 * ticks_));

    //Reposition logo
    if (up_ && logoY_ != (logoSize_ / 1.5))
    {
        logoY_ = ((8 * logoY_) + (logoSize_ / 1.5)) / 9;
    }
    if (down_ && logoY_ != (height() - (logoSize_ / 1.5)))
    {
        logoY_ = ((8 * logoY_) + (height() - (logoSize_ / 1.5))) / 9;
    }
    if (left_ && logoX_ != (logoSize_ / 1.5))
    {
        logoX_ = ((8 * logoX_) + (logoSize_ / 1.5)) / 9;
    }
    if (right_ && logoX_ != (width() - (logoSize_ / 1.5)))
    {
        logoX_ = ((8 * logoX_) + (width() - (logoSize_ / 1.5))) / 9;
    }

    if (!up_ && !down_)
    {
        logoY_ = ((8 * logoY_) + (height() / 2)) / 9;
    }
    if (!left_ && !right_)
    {
        logoX_ = ((8 * logoX_) + (width() / 2)) / 9;
    }

    //Repaint
    repaint();

    //Change cursor
    QLineF line{ QLineF{QPoint{logoX_, logoY_}, cursor().pos() - pos()} };
    int cursor_dist{ static_cast<int>(floor(line.length())) };

    if (cursor_dist < circleRadius_-logoSize_ / 32)
    {
        setCursor(Qt::PointingHandCursor);
    }
    else if (cursor_dist < circleRadius_ * 1.5 && picking_)
    {
        setCursor(Qt::BlankCursor);
    }
    else
    {
        unsetCursor();
    }
}

void FlagWidget::paintEvent(QPaintEvent *)
{
    QPainter painter{ this };
    //Draw background
    QPixmap pixmap{ pixmap.fromImage(background_) };
    painter.drawPixmap(0, 0, width(), height(), pixmap);

    painter.setRenderHint(QPainter::Antialiasing);

    if (picking_)
    {

        //Calculate color
        QLineF line{ QPoint(logoX_, logoY_), cursor().pos() - pos() };
        int cursor_dist{ static_cast<int>(floor(line.length())) };
        bright_ = 2 * (cursor_dist - (circleRadius_ + circleRadius_ / 16)) / (circleRadius_ / 768);

        if (bright_ < 0)
            bright_ = 0;

        if (bright_ > 255)
            bright_ = 255;

        satura_ = 255 - (2 * (cursor_dist - (circleRadius_ + circleRadius_ / 4)) / (circleRadius_ / 768) - 128);

        if (satura_ > 255)
            satura_ = 255;

        QPixmap pixmap{ pixmap.fromImage(picker_) };
        QBrush pickbrush{ pixmap.scaled(circleRadius_ * 3, circleRadius_ * 3) };

        pickbrush.setTransform(QTransform().translate(logoX_ - (circleRadius_ * 1.5f),
                                                      logoY_ - (circleRadius_ * 1.5f)));
        painter.setBrush(pickbrush);
        painter.setPen(Qt::NoPen);

        painter.drawEllipse({ logoX_, logoY_ },
                            circleRadius_ * 1.5f,
                            circleRadius_ * 1.5f);
        //Draw magnifier
        if (cursor_dist > circleRadius_ - logoSize_ / 32 && cursor_dist < circleRadius_ * 1.5f)
        {
            setCursor(Qt::BlankCursor);
            QColor hovercolor{};
            hovercolor.setHsv(line.angle(),satura_, bright_);
            painter.setBrush(QBrush(hovercolor));

            painter.drawEllipse(QPointF(cursor().pos() - pos()),
                                circleRadius_ / 5, circleRadius_ / 5);
        }
    }

    //Draw logo circle
    painter.setPen(QPen(QBrush(QColor(   0,   0,   0, 255)), logoSize_ / 16));
    painter.setBrush(   QBrush(QColor( 255, 255, 255, 239 + (16 * sin((0.03f * ticks_ + 1.5f))) )));
    painter.drawEllipse({ logoX_, logoY_ },
                        circleRadius_, circleRadius_);
    //Draw SVG sail
    rendsvg_.render(&painter, { logoX_ - (logoSize_ / 2),
                               logoY_ - (logoSize_ / 2),
                               logoSize_,
                               logoSize_ });
    //Draw help text
    if (displayHelp_)
    {
        help_.drawContents(&painter, { 0, 0, width(), height() } );
    }
}

void FlagWidget::keyPressEvent(QKeyEvent* e)
{
    int min{ std::min(width(), height()) };
    
    switch (e->key())
    {
    case Qt::Key_F1: displayHelp_ = !displayHelp_;
    break;
    case Qt::Key_Up   : up_    = !up_   ; down_  = false;
    break;
    case Qt::Key_Down : down_  = !down_ ; up_    = false;
    break;
    case Qt::Key_Left : left_  = !left_ ; right_ = false;
    break;
    case Qt::Key_Right: right_ = !right_; left_  = false;
    break;
    case Qt::Key_P: paused_ = !paused_;
    break;
    case Qt::Key_B: logoSize_ = min * 0.9f;
    break;
    case Qt::Key_N: logoSize_ = min / 2.0f;
    break;
    case Qt::Key_M: logoSize_ = min / 4.0f;
    break;
    case Qt::Key_C: picking_ = !picking_;
    break;
    case Qt::Key_Q: close();
    break;
    case Qt::Key_F:
        if (windowState() == Qt::WindowFullScreen)
            setWindowState(Qt::WindowNoState);
        else
            setWindowState(Qt::WindowFullScreen);

    break;
    default: break;
    }

}

void FlagWidget::wheelEvent(QWheelEvent* e)
{
    logoSize_ += 0.1f * e->delta();
    
    if (logoSize_ < 32)
        logoSize_ = 32;
}

void FlagWidget::mousePressEvent(QMouseEvent* e)
{
    if (e->button() == Qt::LeftButton)
    {
        //Relative cursor position
        QLineF line{ { logoX_, logoY_ }, { e->x(), e->y() } };
        float cursor_dist{ line.length() };

        prevPos_ = e->globalPos();

        //Check color wheel
        if (cursor_dist < circleRadius_ - logoSize_ / 32)
        {
            picking_ = !picking_;
        }
        //Set color
        else if (cursor_dist > circleRadius_ - logoSize_ / 32 &&
                 cursor_dist < circleRadius_ * 1.5f && picking_)
        {
            flagColor_.setHsv(line.angle(),satura_,bright_);
        }
        //Check for dragging actions
        else if (!(windowState()==Qt::WindowFullScreen))
        {
            if(e->x() > (width() - 64) && e->y() > (height() - 64))
            {
                scaling_ = true;
            }
            else
            {
                moving_ = true;
            }
        }
    }
}
void FlagWidget::mouseReleaseEvent(QMouseEvent* e)
{
    if(e->button() == Qt::LeftButton)
    {
        moving_ = scaling_ = false;
    }
}
void FlagWidget::mouseMoveEvent(QMouseEvent* e)
{
    if (e->buttons().testFlag(Qt::LeftButton) && moving_)
    {
        QPointF dif{ e->globalPos() - prevPos_ };

        setGeometry(x() + dif.x(),
                          y() + dif.y(),
                          width(),
                          height());

        prevPos_ = e->globalPos();
    }

    if (e->buttons().testFlag(Qt::LeftButton) && scaling_)
    {
        QPointF dif{ e->globalPos() - prevPos_ };

        resize({ width()  + dif.x(),
                 height() + dif.y() });

        prevPos_ = e->globalPos();
    }
}

float FlagWidget::sine(float x)
{
    x = cycle(x, -M_PI, M_PI);
    float sin{};

    if (x < 0)
        sin = 1.27323954f * x + 0.405284735f * x * x;
    else
        sin = 1.27323954f * x - 0.405284735f * x * x;

    if (sin < 0)
        sin = 0.225f * (sin * -sin - sin) + sin;
    else
        sin = 0.225f * (sin *  sin - sin) + sin;

    return sin;
}
float FlagWidget::cycle(float x, float min, float max)
{
    float res{ x };

    if (min > max)
    {
        float temp{ min };

        min = max;
        max = temp;
    }

    float range{ max - min };

    if (x < min)
        res += range * abs(ceil((min - x) / range));
    else if (x > max)
        res -= range * abs(ceil((x - max) / range));

    return res;
}
