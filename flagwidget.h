#ifndef PIXELWIDGET_H
#define PIXELWIDGET_H

#include <QWidget>
#include <QImage>
#include <QSvgRenderer>
#include <QTextDocument>

class FlagWidget: public QWidget
{
    Q_OBJECT

public:
    explicit FlagWidget(QWidget* parent = nullptr);

private slots:
    void paintEvent(QPaintEvent* e);
    void keyPressEvent(QKeyEvent*   e);
    void wheelEvent(QWheelEvent* e);
    void mousePressEvent(QMouseEvent* e);
    void mouseReleaseEvent(QMouseEvent* e);
    void mouseMoveEvent(QMouseEvent* e);
    void onTimer();

private:
    QSvgRenderer rendsvg_;
//    std::vector<float> sine;

    QImage background_;
    QImage picker_;

    QTextDocument help_;
    bool displayHelp_;

    float logoSize_;
    float logoX_;
    float logoY_;
    float epiX_;
    float epiY_;
    float circleRadius_;
    unsigned ticks_;
    QColor flagColor_;
    int bright_;
    int satura_;

    float dist_;
    float wave_;
    int dith_;

    bool fullscreen_;
    bool paused_;
    bool up_;
    bool down_;
    bool left_;
    bool right_;
    bool moving_;
    bool scaling_;
    bool picking_;
    QPointF prevPos_;

    float sine(float x);
    float cycle(float x, float min, float max);
};

#endif // PIXELWIDGET_H
