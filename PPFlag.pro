#-------------------------------------------------
#
# Project created by QtCreator 2014-05-08T02:47:12
#
#-------------------------------------------------

QMAKE_CXXFLAGS += -std=c++1y

QT       += core gui svg

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ppflag
TEMPLATE = app


SOURCES += \
    main.cpp \
    flagwidget.cpp

HEADERS  += \
    flagwidget.h

FORMS    +=

RESOURCES += \
    ppflag.qrc

OTHER_FILES += \
    ideas.txt
