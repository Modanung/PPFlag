#include "flagwidget.h"
#include <QApplication>
#include <QIcon>
#include <QIconEngine>

int main(int argc, char* argv[])
{
    QApplication a(argc, argv);
    FlagWidget w;
    w.setWindowFlags(Qt::Window | Qt::FramelessWindowHint);
    w.setWindowTitle("PP Flag");
    w.setWindowIcon(QIcon(":/icon.svg"));
    w.show();

    return a.exec();
}
