PPFlag
======

A PirateParty flag application

![Screenshot](Screenshots/Screenshot_2016-03-24_02-49-39.png)

 Input | Action
-------|--------
 F1 | Display help
 C | Toggle color picker
 Arrow keys | Position logo
 Scroll wheel | Change logo size
 B | Big logo
 N | Normal size logo
 M | Mini logo
 F | Toggle fullscreen
 P | Pause wave
 Q | Quit
